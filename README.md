# Slurm in Docker

A Docker container (based on Fedora) running the Slurm batch system.
It mainly contains various tools used to test
[GitLab HPC Driver](https://gitlab.hzdr.de/fwcc/gitlab-hpc-driver).

## Setup

The container is set up to automatically detect your hardware on start-up. It will provide four
compute nodes that share the specification of the platform the container is running on (number of
cores, hyperthreading, ...). The _cluster_ is called `sid` (Slurm in Docker) and it has one
partition, `sidp`, containing the compute nodes `sidc[1-4]`. The Slurm database daemon which writes
accounting information to a MySQL database is enabled as well.

Starting the container takes a couple of seconds as the services need to be started at first.
By default, if no command was passed to the container, the login shell of the _ca_ user is run.
This user can run `sudo` without a password.
The table below shows all available users.

| user | name           | uid/gid | groups           |
|------|----------------|---------|------------------|
| ca   | Cluster Admin  | 1000    | ca, users, wheel |
| cu1  | Cluster User 1 | 2001    | cu1, users       |
| cu2  | Cluster User 2 | 2002    | cu2, users       |

## Running the container

If you don't want to build the container yourself, you can pull it from HZDR's registry. (Remember
to log in and out!)

```bash
docker login registry.hzdr.de
docker pull registry.hzdr.de/fwcc/slurm-in-docker
docker logout registry.hzdr.de
```

To run the container use this command:

```bash
docker run -ti --rm registry.hzdr.de/fwcc/slurm-in-docker
```

The image is updated weekly.

## Building the container

If you have Docker set up on your system, you can easily build the container by executing `make`.
This will run the default target `build` which builds and tags the image for you. The first build
may take a while.

The default name of the container is `registry.hzdr.de/fwcc/slurm-in-docker`. If you want to
choose a shorter name, you can pass it as an argument when building the container, e.g.

```bash
make NAME=slurm
```

Verify the build worked:

```bash
docker image ls
```

## Using it in your CI job

... is as easy as specifying `image: registry.hzdr.de/fwcc/slurm-in-docker`. GitLab will pull it
from the registry for you and place your job inside of a new container. Here is an example job:

```yml
my_job:
    image: registry.hzdr.de/fwcc/slurm-in-docker
    script:
        - srun -n 2 hostname
```

## Running other programs

You can pass any program name to the container. At the moment, there are not a lot of programs
installed on it, though. Feel free to submit an issue or a merge request if you have any
suggestions.

```bash
docker run -ti --rm registry.hzdr.de/fwcc/slurm-in-docker ps -ef --forest
```
