NAME      = slurm-in-docker
TAG       = latest
CONTAINER = $(NAME):$(TAG)

.PHONY: build push run

build: Dockerfile docker-entrypoint.sh
	docker build -t $(CONTAINER) .

push: build
	docker push $(CONTAINER)

run: build
	docker run -ti --rm $(CONTAINER)
